import java.util.HashMap;

public class Polinom {
    HashMap<Integer, Monom> storage;
    public Polinom(){
        storage  =  new HashMap<Integer, Monom>();
    }
    public Polinom(String text){
        storage =  new HashMap<>();
        int start, power, sign;
        double coef;
        for(int i=0;i<text.length();){
            sign = 0;
            if(text.charAt(i)=='-'){sign = 1;i++;}
            else if(text.charAt(i) == '+') {sign = 0;i++;}
            start = i;
            while(i<text.length() && text.charAt(i)>='0' && text.charAt(i)<='9') i++;
            if(start == i) coef = 1;
            else coef = Double.parseDouble(text.substring(start, i));
            if(sign == 1)coef = 0 - coef;
            if(i >= text.length())
            {
                Monom monom = new Monom(coef, 0);
                storage.put(monom.getPower(), monom);
                break;
            }
            power=0;
            if((i<text.length()) && (text.charAt(i)=='X' || text.charAt(i)=='x')){ i++; power=1;}
            if(i<text.length() && text.charAt(i)=='^')i++;
            start = i;
            while(i<text.length() && text.charAt(i)>='0' && text.charAt(i)<='9')i++;
            if(start!=i) power = Integer.parseInt(text.substring(start, i));
            Monom monom = new Monom(coef, power);
            storage.put(monom.getPower(), monom);
        }
    }
    public void clone(Polinom p){
        this.storage.clear();
        for(Integer i: p.storage.keySet()){
            this.storage.put(i, p.storage.get(i));
        }
    }
    public Polinom adition(Polinom p){
        Polinom sum = new Polinom();
        Monom aux1,aux2;
        for(Integer i: this.storage.keySet()){
            sum.storage.put(i,this.storage.get(i));
        }
        for(Integer i: p.storage.keySet()){
            if(sum.storage.containsKey(i)){
                aux1 = sum.storage.get(i);
                aux2 = p.storage.get(i);
                aux1 = aux1.add(aux2);
                sum.storage.remove(i);
                if(aux1.getCoef()!=0)
                    sum.storage.put(i,aux1);
            }
            else{
                aux1 = p.storage.get(i);
                sum.storage.put(i, aux1);
            }
        }
        return sum;
    }
    public Polinom subtraction(Polinom p){
        Polinom sub = new Polinom();
        Monom aux1,aux2;
        for(Integer i: this.storage.keySet()){
            sub.storage.put(i,this.storage.get(i));
        }
        for(Integer i: p.storage.keySet()){
            if(sub.storage.containsKey(i)){
                aux1 = sub.storage.get(i);
                aux2 = p.storage.get(i);
                aux1 = aux1.sub(aux2);
                sub.storage.remove(i);
                if(aux1.getCoef()!=0)
                    sub.storage.put(i,aux1);
            }
            else{
                aux1 = new Monom(p.storage.get(i).getCoef(), p.storage.get(i).getPower());
                aux1.setCoef(0-aux1.getCoef());
                sub.storage.put(i, aux1);
            }
        }
        return sub;
    }
    public Polinom multiplication(Polinom p){
        Polinom mul = new Polinom();
        Monom aux1,aux2;
        for(Integer i: p.storage.keySet()){
            for(Integer j: this.storage.keySet()){
                aux1 = p.storage.get(i);
                aux2 = this.storage.get(j);
                aux1 = aux1.mul(aux2);
                if(mul.storage.containsKey(aux1.getPower())){
                    aux2 = mul.storage.get(aux1.getPower());
                    mul.storage.remove(aux1.getPower());
                    aux1 = aux1.add(aux2);
                    mul.storage.put(aux1.getPower(), aux1);
                }
                else{
                    mul.storage.put(aux1.getPower(), aux1);
                }
            }
        }
        return mul;
    }
    public Polinom division(Polinom p, Polinom rem){
        Polinom q = new Polinom();
        Polinom help = new Polinom();
        Polinom r = new Polinom();
        int degreep = p.degree();
        for(Integer i: this.storage.keySet()){
            r.storage.put(i, new Monom(this.storage.get(i).getCoef(), this.storage.get(i).getPower()));
        }
        int degreer = r.degree();
        Monom aux1,aux2;
        while(!r.storage.isEmpty() && degreer>=degreep){
            aux1 = r.storage.get(degreer).div(p.storage.get(degreep));
            q.storage.put(aux1.getPower(), aux1);
            help.storage.clear();
            for(Integer i: p.storage.keySet()){
                aux2  = aux1.mul(p.storage.get(i));
                help.storage.put(aux2.getPower(), aux2);
            }
            r = r.subtraction(help);
            degreer = r.degree();
        }
        for(Integer i: r.storage.keySet()){
            rem.storage.put(i, new Monom(r.storage.get(i).getCoef(), r.storage.get(i).getPower()));
        }
        return q;
    }
    public Polinom derivation(){
        Monom aux;
        Polinom deriv = new Polinom();
        for(Integer i: this.storage.keySet()){
            aux = this.storage.get(i);
            aux  = aux.derivate();
            if(aux.getPower()>=0)
                deriv.storage.put(aux.getPower(), aux);
        }
        return deriv;
    }
    public Polinom integration(){
        Monom aux;
        Polinom deriv = new Polinom();
        for(Integer i: this.storage.keySet()){
            aux = this.storage.get(i);
            aux  = aux.integrate();
            deriv.storage.put(aux.getPower(), aux);
        }
        return deriv;
    }
    public String toString(){
        if(this.storage.isEmpty())return "0";
        StringBuilder str =  new StringBuilder();
        for(Integer i: this.storage.keySet()){
            str.append(this.storage.get(i).toString());
        }
        return str.toString();
    }
    private int degree(){
        int degree=0;
        for(Integer i: this.storage.keySet()){
            if(degree < i)degree = i;
        }
        return degree;
    }
    public double calculateValue(int x){
        double value = 0;
        Monom aux;
        for(Integer i: this.storage.keySet()){
            aux = this.storage.get(i);
            value += aux.getCoef()*Math.pow(x, i);
        }
        return value;
    }
}
