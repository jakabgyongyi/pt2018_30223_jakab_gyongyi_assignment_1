import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class Calculator {
    private Stage window;
    private Scene scene;
    private BorderPane layout;
    private VBox topmenu, leftmenu, centermenu;
    private Label label1, label2;
    private TextField text1, text2;
    private Button button1,button2,but1,but2,but3,but4,but5,but6,view;
    private TextArea  result;
    public void initialize(){
        window = new Stage();
        window.setTitle("POLInomial Calculator");
        layout = new BorderPane();
        topmenu = new VBox();
        layout.setTop(topmenu);
        leftmenu = new VBox();
        layout.setLeft(leftmenu);
        centermenu = new VBox();
        layout.setCenter(centermenu);
        label1 = new Label("Polinom 1");
        label2 = new Label("Polinom 2");
        text1 = new TextField();
        text1.setPromptText("4X^3+X^2+6");
        text1.setMaxWidth(500);
        text2 = new TextField();
        text2.setPromptText("X^2+1");
        text2.setMaxWidth(500);
        button1 = new Button("Parse");
        button2 = new Button("Parse");
        topmenu.getChildren().addAll(label1, text1, button1, label2,text2,button2);
        topmenu.setAlignment(Pos.CENTER);
        topmenu.setSpacing(10);
        but1 = new Button("+");
        but2 = new Button("-");
        but3 = new Button("*");
        but4 = new Button("/");
        but5 = new Button("Integrate");
        but6 = new Button("Derivate");
        leftmenu.getChildren().addAll(but1, but2, but3, but4,but5,but6);
        leftmenu.setAlignment(Pos.CENTER);
        leftmenu.setSpacing(10);
        result = new TextArea();
        result.setMaxWidth(400);
        result.setMaxHeight(150);
        view = new Button("View on xOy");
        centermenu.setAlignment(Pos.CENTER);
        centermenu.setSpacing(10);
        centermenu.getChildren().addAll(result,view);
        scene = new Scene(layout, 640, 480);
        window.setScene(scene);
        window.setResizable(false);
    }
    public void display(){
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();

        button1.setOnAction(e->{
            if(!text1.getText().isEmpty()){
                Polinom result  = new Polinom(text1.getText());
                p1.clone(result);
                System.out.println(p1);
            }
        });
        button2.setOnAction(e->{
            if(!text2.getText().isEmpty()){
                Polinom result = new Polinom(text2.getText());
                p2.clone(result);
                System.out.println(p2);
            }
        });
        but1.setOnAction(e->{
            if(!p1.storage.isEmpty() && !p2.storage.isEmpty()){
                Polinom res = p1.adition(p2);
                result.setText("P1(x) = "+p1+"   +\n"+"P2(x) = "+p2+"   =\n"+"R(x) = "+res);
            }
        });
        but2.setOnAction(e->{
            if(!p1.storage.isEmpty() && !p2.storage.isEmpty()){
                Polinom res = p1.subtraction(p2);
                result.setText("P1(x) = "+p1+"   -\n"+"P2(x) = "+p2+"   =\n"+"R(x) = "+res);
            }
        });
        but3.setOnAction(e->{
            if(!p1.storage.isEmpty() && !p2.storage.isEmpty()){
                Polinom res = p1.multiplication(p2);
                result.setText("P1(x) = "+p1+"   *\n"+"P2(x) = "+p2+"   =\n"+"R(x) = "+res);
            }
        });
        but4.setOnAction(e->{
            if(!p1.storage.isEmpty() && !p2.storage.isEmpty()){
                Polinom r = new Polinom();
                Polinom res = p1.division(p2, r);
                result.setText("P1(x) = "+p1+"   /\n"+"P2(x) = "+p2+"   =\n"+"Q(x)="+res+" + \n"+ "R(x)="+r);
            }
        });
        but5.setOnAction(e->{
            if(!p1.storage.isEmpty()){
                Polinom res = p1.integration();
                result.setText("Integrate of : P(x)= "+p1+"   is: \n"+res+"+C");
            }
        });
        but6.setOnAction(e->{
            if(!p1.storage.isEmpty()){
                Polinom res = p1.derivation();
                result.setText("Derivate of : P(x)= "+p1+"   is: \n"+res);
            }
        });
        view.setOnAction(e->{
            Coordonate coord = new Coordonate();
            coord.initialize();
            coord.display(p1);
        });
        window.show();
    }
}
