public class Monom {
    private double coef;
    private int power;
    public Monom(double coef, int power) {
        this.coef = coef;
        this.power = power;
    }
    public double getCoef() {
        return coef;
    }
    public void setCoef(double coef) {
        this.coef = coef;
    }
    public int getPower() {
        return power;
    }
    public void setPower(int power) {
        this.power = power;
    }
    public Monom add(Monom obj){
        if(this.power!=obj.getPower())
            return null;
        return new Monom(this.getCoef()+obj.getCoef(), obj.getPower());
    }
    public Monom sub(Monom obj){
        if(this.power!=obj.getPower())
            return null;
        return new Monom(this.getCoef()-obj.getCoef(), obj.getPower());
    }
    public Monom mul(Monom obj){
        return new Monom(this.getCoef()*obj.getCoef(), this.getPower()+obj.getPower());
    }
    public Monom div(Monom obj){
        return new Monom(this.getCoef()/obj.getCoef(), this.getPower()-obj.getPower());
    }
    public Monom integrate(){
        return new Monom(this.getCoef()/(this.getPower()+1), this.getPower()+1);
    }
    public Monom derivate(){
        return new Monom(this.getCoef()*this.getPower(), this.getPower()-1);
    }
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        double x = this.getCoef();
        int y = this.getPower();
        if(y > 1) {
            if (x > 0  && x!=1) str.append("+").append(x).append("X^").append(y);
            if (x < 0 && x!=-1) str.append(x).append("X^").append(y);
            if (x == 1) str.append("+X^").append(y);
            if (x == -1) str.append("-").append("X^").append(y);
        }
        if(y == 0){
            if (x > 0 && x!=1) str.append("+").append(x);
            if (x < 0 && x!=-1) str.append(x);
            if (x == 1) str.append("+1");
            if (x == -1) str.append("-1");
        }
        if(y == 1){
            if (x > 0 && x!=1) str.append("+").append(x).append('X');
            if (x < 0 && x!=-1) str.append(x).append('X');
            if (x == 1) str.append("+X");
            if (x == -1) str.append("-X");
        }
        return str.toString();

    }
}
