import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;

public class Coordonate {
    private Stage window;
    private Path path;
    private Group root;
    private Scene scene;
    private Line AxaX;
    private Line AxaY;
    public void initialize(){
        window = new Stage();
        path = new Path();
        root = new Group(path);
        AxaX = new Line(240, 0, 240, 480);
        AxaX.setStroke(Color.BLACK);
        AxaY = new Line(0, 240, 480, 240);
        AxaY.setStroke(Color.BLACK);
        root.getChildren().addAll(AxaX, AxaY);
        for(int i=24;i<480;i+=24){
            Line line = new Line(i, 237, i, 243);
            line.setStroke(Color.BLACK);
            root.getChildren().add(line);
        }
        for(int i=24;i<480;i+=24){
            Line line = new Line(237, i, 243, i);
            line.setStroke(Color.BLACK);
            root.getChildren().add(line);
        }
        scene = new Scene(root, 480, 480);
        window.setTitle("xOy");
        window.setScene(scene);
        window.setResizable(false);
    }
    public void display(Polinom poli){
        int y1,y2;
        for(int i=0;i<10;i++){
            y1 = (int)poli.calculateValue(i);
            y2 = (int)poli.calculateValue(i+1);
            Line line = new Line(i*24+240, 240-y1*24, (i+1)*24+240, 240-y2*24);
            line.setStroke(Color.ROYALBLUE);
            root.getChildren().add(line);
        }
        for(int i=0;i>-10;i--){
            y1 = (int)poli.calculateValue(i);
            y2 = (int)poli.calculateValue(i-1);
            Line line = new Line(i*24+240, 240-y1*24, (i-1)*24+240, 240-y2*24);
            line.setStroke(Color.ROYALBLUE);
            root.getChildren().add(line);
        }
        window.show();

    }
}
