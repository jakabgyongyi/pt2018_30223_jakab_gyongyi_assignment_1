import org.junit.Test;

import static org.junit.Assert.*;

public class PolinomTest {

    @Test
    public void adition() {
        Polinom p1 = new Polinom("X^4-12X^2+3X");
        Polinom p2 = new Polinom("-X^4+12X^2-3X");
        Polinom result = p1.adition(p2);
        System.out.println(result);
    }

    @Test
    public void subtraction() {
        Polinom p1 = new Polinom("X^4-12X^2+3X");
        Polinom p2 = new Polinom("X^4-12X^2+3X");
        Polinom result = p1.subtraction(p2);
        System.out.println(result);
    }

    @Test
    public void multiplication() {
        Polinom p1 = new Polinom("X^4-12X^2+3X");
        Polinom p2 = new Polinom("1");
        Polinom result = p1.multiplication(p2);
        System.out.println(result);
    }

    @Test
    public void division() {
        Polinom p1 = new Polinom("X^4-12X^2+3X");
        Polinom p2 = new Polinom("-8X^2+4X-6");
        Polinom r = new Polinom();
        Polinom result = p1.division(p2, r);
        System.out.println("Q(x): "+result+"\n R(x): "+r);
    }

    @Test
    public void derivation() {
        Polinom p1 = new Polinom("X^4-12X^2+3X");
        Polinom result = p1.derivation();
        System.out.println(result);
    }

    @Test
    public void integration() {
        Polinom p1 = new Polinom("X^4-12X^2+3X");
        Polinom result = p1.integration();
        System.out.println(result);
    }
}